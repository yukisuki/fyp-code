#!/usr/bin/env python3

import psutil, json

logicalCPUCount = psutil.cpu_count()
physicalCPUCount = psutil.cpu_count(logical=False)
CPULoadPerCore = psutil.cpu_percent(percpu=True)
totalCPULoad = psutil.cpu_percent()

printData = json.dumps({
    "Logical CPU count": logicalCPUCount,
    "Physical CPU count": physicalCPUCount,
    "Load per CPU": CPULoadPerCore,
    "Total CPU Load": totalCPULoad
})

print(printData)
