#!/usr/bin/env python3

import psutil, json

memoryObject = psutil.virtual_memory()
memoryTotal = memoryObject.total
memoryUsed = memoryObject.used
memoryFree = memoryObject.free
memoryPercentUsage = memoryObject.percent

swapMemoryObject = psutil.swap_memory()
swapMemoryTotal = swapMemoryObject.total
swapMemoryUsed = swapMemoryObject.used
swapMemoryFree = swapMemoryObject.free



printData = json.dumps({
    "Memory Installed": memoryTotal,
    "Memory Used": memoryUsed,
    "Memory Free": memoryFree,
    "Swap Memory Installed": swapMemoryTotal,
    "Swap Memory Used": swapMemoryUsed,
    "Swap Memory Free": swapMemoryFree,

})

print(printData)
