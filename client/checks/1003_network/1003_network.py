#!/usr/bin/env python3

import psutil, json

networkData = psutil.net_io_counters()

printData = json.dumps({
    "Total Sent": networkData.bytes_sent,
    "Total Recieved": networkData.bytes_recv,
    "Sent packet erorr count": networkData.errout,
    "Recieved packet erorr count": networkData.errin

})

print(printData)
