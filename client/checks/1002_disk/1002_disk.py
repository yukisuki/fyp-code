#!/usr/bin/env python3

import psutil, json

diskData = psutil.disk_usage('/')

printData = json.dumps({
    "Total": diskData.total,
    "Percent used": diskData.percent,
    "Disk Used": diskData.used,
    "Disk Free": diskData.free

})

print(printData)
