#!/usr/bin/env python3

from threading import Thread
import sys
import os

# import all sub processes
from services.clientService import clientService
from services.commService import commService
from configurator import configurator

# config
COMM_PORT = 2007

def start(config):
    # data thread
    # responsible for everything database
    # dataThread = dataService()
    # dataThread.start()

    # api thread
    # responsible for communcating with the main server
    clientServiceThread = clientService(config)
    clientServiceThread.start()

    # client thread
    # responsible for automatic checks
    commServiceThread = commService(COMM_PORT)
    commServiceThread.start()

    while True:
        cmd = input('~')
        if cmd == None:
            continue

        if cmd == 'q':
            os._exit(0)
        else:
            print('unkown command. Type h for help')

if __name__ == '__main__':
    config = configurator()

    if (config.checkConfig() == False):
        config.createConfig()
        print('restarting application')
        os._exit(0)
    else:
        start(config)
