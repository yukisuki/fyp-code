#!/usr/bin/env python3

import sys, os, json, urllib3, socket
from services.includes.httpClient import httpClient

class configurator:

    def __init__(self):
        self._configPath = 'config/main.json'
        urllib3.disable_warnings()
        self._client = httpClient()
        self._config = None
        self._setup()

    def checkConfig(self):
        try:
            with open(self._configPath, 'r') as configFile:
                self._config = json.loads(configFile.read())
        except FileNotFoundError:
            print('Config file not found. Creating new config file')
            return False
        except:
            print('Config corrupt, creating new config')
            return False

    def createConfig(self):
        if (self._mainServerConnection()):
            print('successfully connected to main server, please copy key from web or server command line')

        if (self._checkKey()):
            print('Client key verified')

        data = {
            "serverIP": self._ip,
            "clientKey": self._clientKey
        }

        try:
            with open(self._configPath, 'w') as configFile:
                json.dump(data, configFile)
                print(configFile)
        except:
            print('Cannot create config file. Check if user has premissions to write.')
            return False

    def getClientKey(self):
        return self._config['clientKey']

    def getServerIP(self):
        return self._config['serverIP']

    def _checkKey(self):
        while True:
            key = input('Pleas enter the Client key from main server console or web api')
            if key == None:
                continue

            data = self._client.checkClientKey(self._ip, key)

            if (data['Message'] == "Client key verified"):
                self._clientKey = key
                return True

    def _mainServerConnection(self):
        while True:
            address = input('Please enter the server IP (Use colon if you use custom port):')
            if address == None:
                continue

            if ":" not in address:
                address = address + ':2009'

            data = self._client.requestClientKey(address)

            if data != False:
                if data["Code"] == 200:
                    self._ip = address
                    print('Key created, check main server log or web interface')
                    return True
                else:
                    print('internal server error')

    def _setup(self):
        if os.path.isdir("config/") != True:
            os.mkdir('config/')
