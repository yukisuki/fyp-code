#!/usr/bin/env python3

import sys, os, json, time
import subprocess

class checkObject():
    def __init__(self, configFileURI):
        self._configFile = None # config python object
        self._lastExecuteTime = 0 # unix timestamp; initial set as 0 to only run once
        self._executeEvery = 0 # int in seconds; initial set as 0 to only run once

        # read the config file
        try:
            with open(configFileURI, 'r') as configFile:
                self._configFile = json.loads(configFile.read())
        except:
            print('Malformed config file `{}`'.format(configFileURI))
            return

        self._configFileLocation = configFileURI # config file location
        self._checkName = self._configFile['check']['name'] # check name
        self._author = self._configFile['check']['author'] # author of check
        self._description = self._configFile['check']['description'] # description of check
        self._checkEntryFile = self._configFile['check']['main'] # enter point for the check

        try:
            if self._configFile['settings'] != None: # if the config contains settings for the check
                if self._configFile['settings']['runEvery'] != None:
                    self._executeEvery = self._configFile['settings']['runEvery'] # get execute every x secs setting
        except:
            pass

        print('New check `{}` registered'.format(self._configFile['check']['name']))

    # get name of the check
    def getName(self):
        if (self._configFile != None):
            return self._checkName

    # run the check and get data back
    def run(self):
        if (self._configFile != None):
            self._lastExecuteTime = int(time.time())
            result = subprocess.run(self._checkEntryFile, stdout=subprocess.PIPE)
            return str(result.stdout.decode().rstrip('\n'))

    #
    def isRepeatable(self):
        if self._executeEvery != 0:
            return True
        else:
            return False

    # check if the check can be ran
    def canBeRun(self):
        if (self._configFile != None):
            currentTime = int(time.time())
            if ((self._lastExecuteTime + self._executeEvery) < currentTime):
                return True
            else:
                return False
