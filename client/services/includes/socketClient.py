#!/usr/bin/env python3

import urllib3, socket, json, sys, ssl, traceback, re, time
from time import sleep

class socketClient():
    def __init__(self, config = None):
        # self._client = urllib3.PoolManager(cert_reqs='CERT_NONE')
        self._clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._clientSocket = ssl.wrap_socket(
            self._clientSocket,
            ssl_version=ssl.PROTOCOL_TLSv1_2,
            ciphers='ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK')
        if config != None:
            self._clientKey = config.getClientKey()
            self._serverIP, self._serverPort = config.getServerIP().split(':')
            self._serverPort = int(self._serverPort)

    def _send(self, sock, method, path, data):
        if not method:
            print('Method parameter required')
            return

        if not path:
            print('Path parameter required')
            return

        if method.lower() == 'post' and not data:
            print('Data parameter required when posting')
            return

        message = '{} {} HTTP/1.1\r\n'.format(method.upper(), path)
        headers = {}
        headers['Server'] = 'Monser Client'
        headers['Content-Type'] = 'application/json'

        if method.lower() == 'post' and data:
            headers['Content-Length'] = str(len(data))

        for (type, value) in headers.items():
            message += type + ': ' + value + '\r\n'
        message += '\r\n/h/e'

        if method.lower() == 'post':
            message += '{}\r\n/m/e'.format(data)

        sock.send(message.encode())

        chunkedMessage = []
        finished = False

        while finished is not True:
            chunk = sock.recv(1024)
            chunkedMessage.append(chunk.decode())
            time.sleep(0.001)

            if b'\r\n/m/e' in chunk:
                finished = True

        # fullMessage = re.sub('(\/m\/e)|(\/h\/e)', '', )
        sock.close()
        return self._processMessage(''.join(chunkedMessage))

    def _processMessage(self, message):
        headers, body = message.split('\r\n/h/e')
        body = re.sub('(\/m\/e)', '', body)

        response = headers.split('\r\n')[:1][0]

        serverHeadersRaw = filter(None, headers.split('\r\n')[1:])
        serverHeaders = {}

        for header in serverHeadersRaw:
            if ":" not in header:
                continue
            key, value = header.split(':', 1)
            serverHeaders[key] = value

        return response, body

    def sendCheckData(self, **data):
        hostName = socket.gethostname()
        serviceName = data.get('serviceName')
        serviceOutput = data.get('serviceOutput').rstrip()


        serviceData = json.dumps({
            "hostName": hostName,
            "serviceName": serviceName,
            "serviceOutput": serviceOutput

        })

        try:
            self._clientSocket.connect((self._serverIP, self._serverPort))
            result = self._send(
                    self._clientSocket,
                    'POST',
                    '/serviceData?key={}'.format(self._clientKey),
                    serviceData
            )

            # print(result)
            return True
        except:
            print(str(sys.exc_info()))
            print(traceback.print_tb(sys.exc_info()[2]))
            print('Connection to main server failed')
            return False
        # finally:
        #     self._clientSocket.close()




    def ping(self):
        try:
            result = self._client.request(
                    'GET',
                    'https://{}/index'.format(self._serverIP)
                )
        except:
            print(str(sys.exc_info()[0]))
            # print(e)
            print('Connection to main server failed')
            return False

        print(result)


    # Functions that do not require client key

    def requestClientKey(self, address):
        hostname = socket.gethostname()
        data = json.dumps({
                "hostname": hostname
            })

        try:
            result = self._client.request(
                    'POST',
                    'https://{}/getclikey'.format(address),
                    headers={'Content-Type': 'application/json'},
                    body=data
                )
        except:
            print('Connection to main server failed')
            return False

        returnData = result.data.decode()

        if self._validateJSON(returnData):
            return json.loads(returnData)
        else:
            return False

    def checkClientKey(self, address, key):
        data = json.dumps({
            "key": key
        })

        try:
            result = self._client.request(
                    'POST',
                    'https://{}/checkclikey'.format(address),
                    headers={'Content-Type': 'application/json'},
                    body=data
                )
        except:
            print('Connection to main server failed')
            return False

        returnData = result.data.decode()

        if self._validateJSON(returnData):
            return json.loads(returnData)
        else:
            return False

    def _validateJSON(self, obj):
        if type(obj) == dict:
            obj = json.dumps(obj)

        try:
            jsonObject = json.loads(obj)
            return True
        except ValueError:
            return False
