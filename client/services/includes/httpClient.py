#!/usr/bin/env python3

import urllib3, socket, json, sys, re

class httpClient():

    def __init__(self, config = None):
        self._client = urllib3.PoolManager(cert_reqs='CERT_NONE')
        if config != None:
            self._clientKey = config.getClientKey()
            self._serverIP = config.getServerIP();

    def sendData(self, **data):
        hostName = socket.gethostname()
        serviceName = data.get('serviceName')
        serviceOutput = data.get('serviceOutput').rstrip()


        serviceData = json.dumps({
            "hostName": hostName,
            "serviceName": serviceName,
            "serviceOutput": serviceOutput

        })

        try:
            result = self._client.request(
                    'POST',
                    'https://{}/serviceData?key={}'.format(self._serverIP, self._clientKey),
                    headers={'Content-Type': 'application/json', 'User-Agent': 'monser client'},
                    body=serviceData
                )
        except:
            print(str(sys.exc_info()[0]))
            print('Connection to main server failed')
            return False

        print(result.status)



    def ping(self):
        try:
            result = self._client.request(
                    'GET',
                    'https://{}/index'.format(self._serverIP)
                )
        except:
            print(str(sys.exc_info()[0]))
            # print(e)
            print('Connection to main server failed')
            return False

        print(result)


    # Functions that do not require client key

    def requestClientKey(self, address):
        hostname = socket.gethostname()
        data = json.dumps({
                "hostname": hostname
            })

        try:
            result = self._client.request(
                    'POST',
                    'https://{}/getclikey'.format(address),
                    headers={'Content-Type': 'application/json'},
                    body=data
                )
        except:
            print('Connection to main server failed')
            return False

        result = result.data.decode()
        returnData = re.sub('(\/m\/e)|(\/h\/e)', '', ''.join(result))

        if self._validateJSON(returnData):
            return json.loads(returnData)
        else:
            return False

    def checkClientKey(self, address, key):
        data = json.dumps({
            "key": key
        })

        try:
            result = self._client.request(
                    'POST',
                    'https://{}/checkclikey'.format(address),
                    headers={'Content-Type': 'application/json'},
                    body=data
                )
        except:
            print('Connection to main server failed')
            return False

        result = result.data.decode()
        returnData = re.sub('(\/m\/e)|(\/h\/e)', '', ''.join(result))

        if self._validateJSON(returnData):
            return json.loads(returnData)
        else:
            return False

    def _validateJSON(self, obj):
        if type(obj) == dict:
            obj = json.dumps(obj)

        try:
            jsonObject = json.loads(obj)
            return True
        except ValueError:
            return False
