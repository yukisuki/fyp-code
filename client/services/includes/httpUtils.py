#!/usr/bin/env python3

import json

class httpUtils:
    def __init__(self):
        self._httpCodes = {
            '200': 'Ok',
            '401': 'Unauthorized',
            '404': 'Not Found',
            '400': 'Bad Request',
            '403': 'Forbidden',
            '500': 'Internal Server Error'
        }


    def getHTTPMessage(self, code):
        return self._httpCodes[str(code)]
