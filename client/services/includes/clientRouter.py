#!/usr/bin/env python3

import json

from services.includes.httpRequest import httpRequest

class clientRouter:
    def __init__(self):
        self._routeFunction = {}
        self._routeMetadata = {}

    def route(self, **kwargs):
        method = kwargs.get('method')
        client = kwargs.get('client')
        request = httpRequest(**{'client': client})

        try:
            path, args = kwargs.get('path').split('?')
        except ValueError:
            path, args = kwargs.get('path'), None

        if path not in self._routeFunction:
            request.notFound()
            return

        if method not in self._routeMetadata[path]:
            request.send(400, {}, 'Method not in function')
            return

        self._routeFunction[path](httpRequest(**{
            'method': method,
            'path': path,
            'args': args,
            'body': kwargs.get('body'),
            'client': client
        }))

    def addRoute(self, routeFunction, **args):
        path = args.get('path')
        self._routeFunction[path] = routeFunction
        self._routeMetadata[path] = args.get('methods')
