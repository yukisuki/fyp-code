#!/usr/bin/env python3

import os, time
from time import sleep
from threading import Thread
from glob import glob
from queue import Queue

# from services.includes.httpClient import httpClient
from services.includes.socketClient import socketClient
from services.includes.checkObject import checkObject

# import threading
#
# def printit():
#   threading.Timer(5.0, printit).start()
#   print "Hello, World!"
#
# printit()
#
# # continue with the rest of your code

class clientService(Thread):
    def __init__(self, config):
        self._config = config
        self._checks = []
        self._setup()
        self._registerChecks()
        self._sendQueue = Queue()
        Thread.__init__(self)

    def run(self):
        print('client service started')
        for check in self._checks:
            checkResult = check.run()
            if checkResult != None:
                self._sendQueue.put({
                        'result': checkResult,
                        'name': check.getName()
                    })

        while True:
            for check in self._checks:
                if check.isRepeatable():
                    if check.canBeRun():
                        checkResult = check.run()
                        if checkResult != None:
                            self._sendQueue.put({
                                    'result': checkResult,
                                    'name': check.getName()
                                })

            if not self._sendQueue.empty():
                data = self._sendQueue.get()
                if data is None:
                    return False
                client = socketClient(self._config)
                result = client.sendCheckData(**{
                        'serviceName': data['name'],
                        'serviceOutput': data['result']

                    })
                if (result):
                    print('{} check successfully executed at {}'.format(data['name'], time.strftime("%H:%M:%S") ))
                else:
                    print('{} check failed to execute at {}'.format(data['name'], time.strftime("%H:%M:%S") ))
                sleep(1)
            sleep(0.01)


    def _ping(self):
        while True:
            print('ping')
            self._client.ping()
            sleep(5)

    def _registerChecks(self):
        checkFolders = glob('checks/*/')
        for checkDirectory in checkFolders:
            configFile = "{}config.json".format(checkDirectory)
            self._checks.append(checkObject(configFile))

    def _setup(self):
        if os.path.isdir("checks/") != True:
            os.mkdir('checks/')
