#!/usr/bin/env python3

import requests
from threading import Thread
from services.includes.socketServer import socketServer
from services.includes.clientRouter import clientRouter

class commService(Thread):

    def __init__(self, apiPort):
        self._port = apiPort
        self._router = clientRouter()
        self._apiServer = socketServer(apiPort, self._router)
        Thread.__init__(self)

    def run(self):
        print('API servie started on port {}'.format(self._port))

        self._router.addRoute(self.index, **{'path': '/index', 'methods': ['POST', 'GET']})
        self._router.addRoute(self.jsonIndex, **{'path': '/index', 'methods': ['GET']})

        self._apiServer.runHTTPS()

    def index(self, req):
        req.send(200, {}, 'test body')

    def jsonIndex(self, req):
        req.sendJSON({'test': {'head': 'item', 'body': 'value'}, 'code': 40, "d": False})
