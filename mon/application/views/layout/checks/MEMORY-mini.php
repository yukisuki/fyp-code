<?php
$MEM_data = (array)$data;

$percentUsed = ((int)$MEM_data['Memory Used'] / (int)$MEM_data['Memory Installed']) * 100;
# 											kb		mb		gb
$totalMem = $MEM_data['Memory Installed'] / 1024 / 1024 / 1024;

?>
<section>
	Total Memory Usage: <?= (int)$percentUsed ?>% of <?php printf("%.2f", $totalMem) ?>Gb
</section>



<!-- Array
(
    [Memory Used] => 9241624576
    [Swap Memory Installed] => 10094637056
    [Swap Memory Used] => 0
    [Memory Free] => 1079083008
    [Memory Installed] => 12470878208
    [Swap Memory Free] => 10094637056
) -->
