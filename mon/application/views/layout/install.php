<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?= $title ?> - Monser</title>
		<?= link_tag(base_url().'assets/css/normalize.css'); ?>
		<?= link_tag(base_url().'assets/css/install.css'); ?>
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="description" content="">
	</head>
	<body>
		<div id="container">
			<section class="mainContent">
				<?= $bodyContent ?>
			</section>
		</div>
	</body>
</html>
