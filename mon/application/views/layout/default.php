<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?= $title ?> - Monser</title>
		<?= link_tag('./assets/css/normalize.css'); ?>
		<?= link_tag('./assets/css/global.css'); ?>
		<?php if (isset($css)) { foreach ($css as $link) { ?>
			<?= link_tag('assets/css/'.$link) ?>
		<?php } } ?>
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="description" content="">
	</head>
	<body>
		<div id="container">
			<?php $this->view('layout/default_sidebar'); ?>
			<section class="mainContent">
				<?= $bodyContent ?>
			</section>
			<?php if (isset($secondaryContent)) { ?>
				<section class="secondaryContent">
					<?= $secondaryContent ?>
				</section>
			<?php } ?>
		</div>
		<?php print_r($js);?>
		<?php if (isset($js)) { foreach ($js as $link) { ?>
			<script type="text/javascript" src="<?= base_url().'assets/js/'.$link ?>"></script>
		<?php } } ?>
	</body>
</html>
