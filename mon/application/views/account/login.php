<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Login</title>
		<?= link_tag(base_url().'assets/css/normalize.css'); ?>
		<?= link_tag(base_url().'assets/css/global.css'); ?>
		<?= link_tag(base_url().'assets/css/account.css'); ?>
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="description" content="">
	</head>
	<body>
		<div id="container">
			<main class="loginForm">
				<header>Monser Web</header>
				<form method="post" action="<?= site_url('account/verify') ?>">
					<input class="formItem" type="text" name="username" placeholder="Username">
					<input class="formItem" type="password" name="password" placeholder="Password">
					<input class="formItem" type="submit" name="submit" value="Login">
				</form>
				<section class="loginFooter">
					Monser &copy; 2018-<?= date('Y'); ?>
				</section>
			</main>
		</div>
	</body>
</html>
