<section class="service">
	<?php foreach ($checks as $check) {
			$data['data'] = $check['serviceOutput'];
			$checkID = (array)$check['_id'];
		?>
		<a class="service-card-link" href="?host/<?= $checkID['$id'] ?>#service">
			<section class="service-card">
				<header><?= $check['hostName'] ?> - <?= $check['serviceName'] ?> @ <?= date('m-d H:i', $check['serviceTime']) ?></header>
				<p><?php $this->view('layout/checks/'.$check['serviceName'].'-mini', $data) ?></p>
			</section>
		</a>
	<?php } ?>
</section>

<!--
Array
(
    [serviceName] => CPU
    [hostName] => fubuki
    [serviceTime] => 1553072634
    [serviceOutput] => stdClass Object
        (
            [Load per CPU] => Array
                (
                    [0] => 0
                    [1] => 33.3
                    [2] => 0
                    [3] => 0
                )

            [Physical CPU count] => 2
            [Total CPU Load] => 25
            [Logical CPU count] => 4
        )

    [_id] => stdClass Object
        (
            [$id] => 5c9201fafee63775cae81ce1
        )

) -->
