<?php $dat = array($window)[0]; $serviceData = $dat['data'][0]; ?>
<section id="data-show">
	<nav id="host-nav" class="host-nav">
		<a href="#host">Host</a>
		<a href="#service">Service</a>
		<a href="#edit">Edit</a>
		<!-- <a href="#services">Services</a> -->
		<!-- <a href="#history">History</a> -->
	</nav>
	<section class="hidden host-data data-widget">
		<h3>Host</h3>

	</section>
	<section class="hidden service-data data-widget">
		<?php
			$viewData['checkData'] = $serviceData['serviceOutput'];
		?>
		<h3>Service</h3>

		<p><?php
		$this->view('layout/checks/'.$serviceData['serviceName'], $viewData) ?></p>


	</section>
	<section class="hidden edit-data data-widget">
		<h3>Editing <?= $serviceData['serviceName'] ?> on <?= $serviceData['hostName'] ?></h3>
		<h4>Edit service settings</h4>
		<form method="post" action="<?= site_url('dashboard/updateservicesettings') ?>">
			<input class="formItem" type="text" name="runEvery" placeholder="300 Seconds">
			<input type="hidden" name="serviceName" value="<?= $serviceData['serviceName'] ?>">
			<input type="hidden" name="hostName" value="<?= $serviceData['hostName'] ?>">
			<input type="hidden" name="redirectURI" value="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>#edit">
			<input class="formItem" type="submit" name="submit" value="Commit">
		</form>
	</section>
</section>
<!--
<section class="hidden services-data data-widget">
	<h3>Services</h3>

</section>

Array
(
[view] => host/show
[data] => Array
(
[0] => Array
	(
		[serviceTime] => 1555846178
		[serviceOutput] => stdClass Object
			(
				[Swap Memory Installed] => 10094637056
				[Memory Free] => 393150464
				[Memory Installed] => 12470882304
				[Memory Used] => 9972469760
				[Swap Memory Free] => 9829441536
				[Swap Memory Used] => 265195520
			)

		[serviceName] => MEMORY
		[hostName] => fubuki
		[_id] => stdClass Object
			(
				[$id] => 5cbc5422fee6372c0b1159ad
			)

	)

)

)
 -->
