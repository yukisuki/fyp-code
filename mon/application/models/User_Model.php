<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {

	public function __construct() {
		parent::__construct();

	}

	public function login() {
		$this->load->library('crypto');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$userData = $this->db->get_where('users', array('username' => $username))->row();


		if ($userData) {
			$pw = $this->crypto->make($password, $userData->salt);

			if ($userData->password === $pw) {
				return $userData->id;


			}

		}

		return false;

	}




}


?>
