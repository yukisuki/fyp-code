<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crypto {

	public function make($string, $salt = '') {
		return hash('sha256', $string . $salt);
	}

	public static function salt($length) {
		return base64_encode(random_bytes($length));
	}

	public static function unique() {
		return self::make(uniqid());
	}

}
