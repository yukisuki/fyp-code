<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template {
	var $template_data = array();
	var $params;

	public function __construct($params) {
	    $this->params = $params;

	}


	function set($name, $value) {
		$this->template_data[$name] = $value;

	}

	function load($view = '', $view_data = array(), $return = FALSE) {
		$this->CI = &get_instance();
		$this->set('bodyContent', $this->CI->load->view($view, $view_data, TRUE));

		if (isset($view_data['window'])) {
			$this->set('secondaryContent', $this->CI->load->view($view_data['window']['view'], $view_data['window']['data'], TRUE));

		}

		if (isset($this->params['css'])) {
			$this->set('css', $this->params['css']);
		}

		if (isset($this->params['js'])) {
			$this->set('js', $this->params['js']);
		}

		return $this->CI->load->view($this->params['default_layout'], $this->template_data, $return);

	}

}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */
