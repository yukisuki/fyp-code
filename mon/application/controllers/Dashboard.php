<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('mongo_db', array('activate'=>'checkData'),'checkDB');
        $templateData = array('default_layout' => 'layout/default', 'css' => array('dashboard.css'), 'js' => array('dashboard/pages.js'));
        $this->load->library('Template', $templateData);

        if (!$this->session->userdata('user')) {
            redirect('account/login');

        }

    }

    public function index() {
        // $this->load->view('dashboard/index');
        $data['title'] = 'Dashboard';
        $data['checks'] = $this->checkDB->order_by(array('serviceTime' => 'desc'))->get('checks');
        if ($_SERVER['QUERY_STRING']) {
            $id = explode('/', $_SERVER['QUERY_STRING'])[1];
            $host = $this->checkDB->where(array("_id" => new MongoDB\BSON\ObjectID($id)))->get('checks');
            $data['window'] = array('view' => 'host/show', 'data' => $host);


        }
        $this->template->load('dashboard/index', $data);

    }

    public function updateservicesettings($value='') {
        $this->load->model('Service_Model');
        $id = $this->Service_Model->update();

        if ($id) {
            redirect('dashboard');

        } else {
            redirect('account');

        }

    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('account/login');

    }


}


?>
