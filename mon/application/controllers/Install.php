<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$templateData = array('default_layout' => 'layout/install');
		$this->load->library('Template', $templateData);

		// if (!$this->session->userdata('user')) {
		// 	redirect('account/login');
		//
		// }

	}

	public function index() {
		$data['title'] = 'Installion';
        $this->template->load('install/index', $data);

	}


}
