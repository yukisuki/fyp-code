<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('user')) {
            redirect('dashboard');

        }


    }

    public function index() {
        $this->load->view('account/login');

    }

    public function login() {
        $this->load->model('User_Model');
        $this->load->view('account/login');

    }

    public function verify() {
        $this->load->model('User_Model');
        $id = $this->User_Model->login();

        if ($id) {
            $this->session->set_userdata('user', $id);
            redirect('dashboard');

        } else {
            redirect('account');

        }


    }

    public function register() {
        $this->load->view('account/register');

    }

    public function complete() {


    }




}


?>
