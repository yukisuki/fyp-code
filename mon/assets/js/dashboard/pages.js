const 	pages = document.getElementById("host-nav"),
		containter = document.getElementById("data-show"),
		hash = window.location.href;

for (let i = 0; i < pages.children.length; i++) {
	page = pages.children[i];

	if (page.href == hash) {
		page.classList.add("active");
		for (let x = 1; x < containter.children.length; x++) {
			dataPage = containter.children[x];
			const pageName = window.location.hash.substr(1),
					pageHash = pageName+'-data';

			if (dataPage.classList.contains(pageHash)) {
				dataPage.classList.remove("hidden");
			}


		}

	}

	page.addEventListener('click', changePage);



}

function changePage() {
	// remove active from all the elements
	for (let i = 0; i < pages.children.length; i++) {
		pages.children[i].classList.remove("active");
	}

	// set clicked element as active
	this.classList.add("active");

	// loop though all pages and make sure they are closed
	for (let x = 1; x < containter.children.length; x++) {
		if (!containter.children[x].classList.contains("hidden")) {
			containter.children[x].classList.add("hidden");
		}
	}

	// show the new page
	for (let x = 1; x < containter.children.length; x++) {

		dataPage = containter.children[x];
		const 	pageName = this.href.split('#')[1],
				pageHash = pageName+'-data';

		if (dataPage.classList.contains(pageHash)) {
			dataPage.classList.remove("hidden");
		}


	}

}
