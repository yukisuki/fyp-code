#!/usr/bin/env python3

# Python std libs
from threading import Thread
import sys
import os

# Custom MonSer libs
from configurator import configurator
from services.sqliteDB import sqliteDB

# import all sub processes
from services.healthService import healthService
from services.apiService import apiService
from services.checkService import checkService
from services.clientService import clientService
from services.apiHTTPService import apiHTTPService


# config
API_PORT = 2008
CLIENT_PORT = 2009

# main start function that starts all threads
# required to run the main server and capture
# data from the clients
def start():
    configDB = sqliteDB()

    # health thread
    # responsible for everything to do with the main server
    # from checking the machine with scripts to checking
    # api / client thread
    healthServiceThread = healthService()
    healthServiceThread.start()

    # api test thread
    # currently disabled as no routes work / have been
    # implemented
    apiHTTPServiceThread = apiHTTPService(API_PORT)
    apiHTTPServiceThread.start()
    print(apiHTTPServiceThread)

    # client thread
    clientServiceThread = clientService(CLIENT_PORT, configDB)
    clientServiceThread.start()

    # wait for user input
    while True:
        cmd = input('~')
        # checking against none stops bug with infinite
        # waiting / scrolling
        if cmd is None:
            continue

        if cmd is 'q':
            os._exit(0)
        else:
            print('unkown command. Type h for help')

# main entry point for the server
# checks if the config is valid
# currently does not work
if __name__ == '__main__':
    config = configurator()

    if config.checkConfig() is False:
        print('config stuff')
    else:
        start()
