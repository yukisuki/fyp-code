#!/usr/bin/env python3

# Python std libs
import sys, os, json

# current unused but implemented
class configurator:

    def __init__(self):

        if os.path.isdir("config/") != True:
            os.mkdir('config/')

        self._configPath = 'config/main.json'
        self._config = None

    def checkConfig(self):
        try:
            with open(self._configPath, 'r') as configFile:
                self._config = json.loads(configFile.read())
        except FileNotFoundError:
            print('Config file not found. Creating new config file')
            # return False
            return True

    # def createConfig(self):
    #
    #     if (_mainServerConnection()):
    #         print('conn\'d')
    #
    #     try:
    #         with open(self._configPath, 'w') as configFile:
    #             print(configFile)
    #
    #     except:
    #         print('Cannot create config file. Check if user has premissions to write.')
    #         return False
    #


    def getConfig(self):
        return self._config

    # def _mainServerConnection(self):
    #     while True:
    #         cmd = input('Please enter the server IP (Use colon if you use custom port):')
    #         if cmd == None:
    #             continue
    #
    #         else:
    #             print('unkown command. Type h for help')
