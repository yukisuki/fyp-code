#!/usr/bin/env python3

from threading import Thread
from services.includes.socketServer import socketServer
from services.includes.httpRouter import httpRouter

# discontinued due to apiHTTPService which functions to
# any http server unlike the custom socketServer below
class apiService(Thread):

    def __init__(self, apiPort):
        self._port = apiPort
        self._router = httpRouter()
        self._apiServer = socketServer(apiPort, self._router)
        Thread.__init__(self)

    def run(self):
        print('API servie started on port {}'.format(self._port))
        self._router.addRoute(self.index, **{'path': '/index', 'methods': ['POST', 'GET']})
        self._router.addRoute(self.getHosts, **{'path': '/hosts/all', 'methods': ['GET']})
        self._apiServer.runHTTPS()


    def index(self, req):
        if req.getMethod() == 'POST':
            req.send(200, {}, 'Post to index')
        else:
            req.send(200, {}, 'Get index')

    def getHosts(self, req):
        req.send(200, {}, 'Getting hosts')
