#!/usr/bin/env python3

import sqlite3

class sqlitedb:
    def __init__(self, fileName):
        self.fileName = fileName

    # get first row by table and params
    def getFirstRow(self, table, params = None):
        queryParams = []
        queryValues = []
        query = 'SELECT * FROM {}'.format(
            table,
        )

        if params != None:
            query += ' WHERE'
            for param in params:
                queryParams.append('{} {} ?'.format(param[0], param[1]))
                queryValues.append(param[2])

        query = '{} {}'.format(
            query,
            ' AND '.join(queryParam for queryParam in queryParams)
        )

        result = self._db.execute(query, queryValues)
        return result.fetchone()

    def getRows(self, table, params = None):
        queryParams = []
        queryValues = []
        query = 'SELECT * FROM {}'.format(
            table
        )

        if params != None:
            query += ' WHERE'
            for param in params:
                queryParams.append('{} {} ?'.format(param[0], param[1]))
                queryValues.append(param[2])

        query = '{} {}'.format(
            query,
            ' AND '.join(queryParam for queryParam in queryParams)
        )

        result = self._db.execute(query, queryValues)
        return result.fetchall()

    # insert a record by table
    def insertRow(self, table, data):
        keys = sorted(data.keys())
        values = [ data[key] for key in keys]

        query = 'INSERT INTO {} ({}) VALUES ({})'.format(
            table,
            ', '.join(keys),
            ', '.join('?' for i in range(len(values)))
        )

        self._db.execute(query, values)
        self._db.commit()

    # update a record by table and ID
    def updateRow(self, table, data):
        print('nop')

    # delete record by table and ID
    def deleteRow(self, ID):
        query = 'DELETE FROM {} WHERE id = ?'.format(table)
        self._db.execute(query, ID)
        self._db.commit()

    # get count of records by table
    def countRows(self, table):
        query = 'SELECT COUNT(*) FROM {}'.format(table)
        cusor = self._db.cursor()
        cusor.execute(query)
        return cusor.fetchone()

    def sql_commit(self, sql, params = ()):
        self._db.execute(sql, params)
        self._db.commit()


    # fileName porperty
    # establishes connection to database when set and closes it when deleted
    @property
    def fileName(self):
        return self._fileName

    @fileName.setter
    def fileName(self, value):
        self._fileName = value
        self._db = sqlite3.connect(value, check_same_thread=False)

    @fileName.deleter
    def fileName(self):
        self._db.close()
        del self._fileName


def runTest():
    dbFile  = ':memory:'
    table   = 'example'

    testData = [
        dict(sample = 'one'),
        dict(sample = 'two'),
        dict(sample = 'three')
    ]

    print('Running tests\n')
    print('Creating db instance\n')
    database = sqldb(dbFile)

    print('Creating test table {}'.format(table))
    database.sql_commit('CREATE TABLE {} (id INTEGER PRIMARY KEY, sample TEXT)'.format(table))

    print('Inserting records into test table {}\n'.format(table))
    for data in testData:
        database.insertRow(table, data)

    print('Get all rows from {} table\n'.format(table))
    database.getRows(table)

    print('Get one row from {} table and compare to testData\n'.format(table))
    toCompare = testData[2].get('sample')
    comapredWith = database.getFirstRow(table, [["sample", "=", "three"]])

    if comapredWith[1] == toCompare:
        print('correct')
    else:
        print('got wrong data')

    print('Getting multiple rows from table {} with multiple wheres'.format(table))



if __name__ == "__main__":
    runTest()
