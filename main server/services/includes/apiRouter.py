#!/usr/bin/env python3

import json

# Custom MonSer libs
from services.includes.apiRequest import apiRequest

# api router class
# this router is based upon the module from
# from http.server import HTTPServer
# this router will not work with the socket server
# unless adapted
class apiRouter:

    # initlize the functions (routes), authorization
    # to onoly authorize certain clients,
    def __init__(self, db = None):
        self._routeFunction = {}
        self._authRoute = {}
        self._routeMetadata = {}
        self._db = db

    # route function will take data from what the client sends
    # and extracts the path, arguments, method etc used and
    # forwards all the data using the ApiRequest class
    # back to the function that was assigned to that path
    def route(self, **kwargs):
        headers = kwargs.get('headers')
        method = kwargs.get('method')
        client = kwargs.get('client')
        request = apiRequest(**{'client': client})

        try:
            path, args = kwargs.get('path').split('?')
            args = self._stringToDict(args)
        except ValueError:
            path, args = kwargs.get('path'), None

        if path not in self._routeFunction:
            request.notFound()
            return

        if method not in self._routeMetadata[path]:
            request.sendJSON({"errorCode": 400, "Message": "Method not in function"}, 400)
            return

        if path in self._authRoute:
            if args is None:
                request.sendJSON({"errorCode": "403", "Message": "No API key supplied"}, 403)
                return
            else:
                if args['key'] is None:
                    request.sendJSON({"errorCode": "403", "Message": "No API key supplied"}, 403)
                    return
                else:
                    key = args['key']
                    ip = client.getpeername()[0]
                    authorized = self._db.checkCLIKey(**{"key": key, "ip": ip})
                    if authorized is not True:
                        request.sendJSON({"errorCode": "403", "Message": "Key does not match client"}, 403)
                        return

        self._routeFunction[path](apiRequest(**{
            'method': method,
            'args': args,
            'body': kwargs.get('body'),
            'client': client
        }))

    # add route function
    #
    def addRoute(self, routeFunction, **args):
        path = args.get('path')
        self._routeFunction[path] = routeFunction
        self._routeMetadata[path] = args.get('methods')
        if args.get('auth') is not None:
            self._authRoute[path] = True

    def _stringToDict(self, args):
        arguments = {}
        parameters = args.split('&')
        for param in parameters:
            key, value = param.split('=')
            arguments[key] = value

        return arguments
