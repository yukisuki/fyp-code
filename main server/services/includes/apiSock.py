#!/usr/bin/env python3
import socket, sys, traceback, time, datetime, ssl

class apiSock:

    #s = ssl.wrap_socket(s, keyfile=None, certfile=None, server_side=False, cert_reqs=ssl.CERT_NONE, ssl_version=ssl.PROTOCOL_SSLv23)

    def __init__(self, port):
        self._httpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._httpsServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self._httpsServer = ssl.wrap_socket(
            self._httpsServer,
            keyfile='./services/includes/cert/private.key',
            certfile='./services/includes/cert/cert.crt',
            server_side=True,
            ssl_version=ssl.PROTOCOL_TLSv1_2,
            ca_certs=None,
            do_handshake_on_connect=True,
            suppress_ragged_eofs=True,
            ciphers='ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK')
        self._port = port

# with context.wrap_socket(sock, server_side=True) as ssock:

    def runHTTPS(self):
        self._httpsServer.bind(('0.0.0.0', self._port))
        self._httpsServer.listen(5)

        # with self._context.wrap_socket(self._httpsServer, service_side=True) as server:
        while True:
            try:
                connection, address = self._httpsServer.accept()

                print('Client ({}) connected'.format(address))
                chunkedMessage = []
                finished = False

                while finished is not True:
                    chunk = connection.recv(1024)
                    print(chunkedMessage)
                    chunkedMessage.append(chunk.decode())
                    if chunk == b'' or b'\r\n\r\n' in chunk:
                        finished = True
                        continue

                connection.send(b'HTTP/1.1 200 Ok\r\nContent-Type: text/html; charset=UTF-8\r\n\r\nHello')
                self.processMessage(''.join(chunkedMessage), connection)


            except:
                print("failed to start thread")
                traceback.print_exc()


    def runHTTP(self):
        self._httpServer.bind(('0.0.0.0', self._port))
        self._httpServer.listen(5)

        while True:
            connection, address = self._httpServer.accept()

            print('Client ({}) connected'.format(address))

            try:
                chunkedMessage = []
                finished = False

                while finished is not True:
                    chunk = connection.recv(1024)
                    print(chunkedMessage)
                    if chunk == b'' or b'\r\n\r\n' in chunk:
                        finished = True
                    chunkedMessage.append(chunk)

                # connection.send(b'HTTP/1.1 200 Ok\r\nContent-Type: text/html; charset=UTF-8\r\n\r\nHello')

                print(chunkedMessage)
                # self.processMessage(''.join(chunkedMessage), connection)


            except:
                print("failed to start thread")
                traceback.print_exc()


    def processMessage(self, message, connection):
        if message.encode() == b'':
            print('Client ({}) shutdown'.format(connection.getpeername()))
            connection.shutdown(2)
            connection.close()
            return

        request = message.split('\r\n')[0:1]
        rawClientParams = filter(None, message.split('\r\n')[1:])
        clientParams = {}
        for param in rawClientParams:
            key, value = param.split(':', 1)
            clientParams[key] = value

        contentType = clientParams.get('Content-Type')
        acceptType = clientParams.get('Accept')

        print(request)


        print('Client ({}) shutdown'.format(connection.getpeername()))
        connection.shutdown(2)
        connection.close()



# sample clientParams
# {
# 	'Host': ' localhost:2008',
# 	'Connection': ' keep-alive',
# 	'Postman-Token': ' a9d8ab89-af6b-47f3-f079-fa053988a3b2 ',
# 	'Cache - Control ': 'no - cache ',
# 	'Sec - Metadata ': 'destination = "", target = subresource,site = cross - site ',
# 	'User - Agent ': 'Mozilla / 5.0(Windows NT 6.3; Win64; x64) AppleWebKit / 537.36(KHTML,like Gecko) Chrome / 69.0 .3497 .100 Safari / 537.36 ',
# 	'Content - Type ': 'application / json ',
# 	'Accept ': ' */*',
# 	'Accept-Encoding': ' gzip, deflate, br',
# 	'Accept-Language': ' en-GB,en;q=0.9,en-US;q=0.8,pl;q=0.7,ja;q=0.6'
# }
