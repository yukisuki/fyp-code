#!/usr/bin/env python3

import json

from services.includes.httpRequest import httpRequest

class httpRouter:
    def __init__(self, db = None):
        self._routeFunction = {}
        self._authRoute = {}
        self._routeMetadata = {}
        self._db = db

    def route(self, **kwargs):
        headers = kwargs.get('headers')
        method = kwargs.get('method')
        client = kwargs.get('client')
        request = httpRequest(**{'client': client})

        try:
            path, args = kwargs.get('path').split('?')
            args = self._stringToDict(args)
        except ValueError:
            path, args = kwargs.get('path'), None

        if path not in self._routeFunction:
            request.notFound()
            return

        if method not in self._routeMetadata[path]:
            request.send(400, {}, 'Method not in function')
            return

        if path in self._authRoute:
            if args == None:
                request.sendJSON({"errorCode": "403", "Message": "No API key supplied"}, 403)
                return
            else:
                if args['key'] == None:
                    request.sendJSON({"errorCode": "403", "Message": "No API key supplied"}, 403)
                    return
                else:
                    key = args['key']
                    ip = client.getpeername()[0]
                    authorized = self._db.checkCLIKey(**{"key": key, "ip": ip})
                    if authorized != True:
                        request.sendJSON({"errorCode": "403", "Message": "Key does not match client"}, 403)
                        return

        self._routeFunction[path](httpRequest(**{
            'method': method,
            'args': args,
            'body': kwargs.get('body'),
            'client': client
        }))

    def addRoute(self, routeFunction, **args):
        path = args.get('path')
        self._routeFunction[path] = routeFunction
        self._routeMetadata[path] = args.get('methods')
        if args.get('auth') != None:
            self._authRoute[path] = True

    def _stringToDict(self, args):
        arguments = {}
        parameters = args.split('&')
        for param in parameters:
            key, value = param.split('=')
            arguments[key] = value

        return arguments
