#!/usr/bin/env python3

import json
from services.includes.httpUtils import httpUtils

class httpRequest:
    def __init__(self, **params):
        self._conn = params.get('client')
        self._method = params.get('method')
        self._path = params.get('path')
        self._args = params.get('args')
        self._body = params.get('body')
        self._utils = httpUtils()

    def send(self, httpResponseCode = 200, headers = {}, body = ''):
        message = self._prepareMessage(httpResponseCode, headers, body)

        self._conn.send(message.encode())

    def badRequest(self, body):
        message = self._prepareMessage(400, {}, body)
        self._conn.send(message.encode())

    def sendJSON(self, jsonObject, httpResponseCode = 200):
        if self._validateJSON(jsonObject):
            message = self._prepareMessage(httpResponseCode, {'Content-Type': 'application/json'}, json.dumps(jsonObject))
            self._conn.send(message.encode())
        else:
            print('JSON Data for request {} is invalid'.format(self._path))


    def notFound(self):
        body = '{"errorCode": "404", "Message": "The requested path cannot be found"}'
        message = self._prepareMessage(404, {'Content-Type': 'application/json'}, body)
        self._conn.send(message.encode())

    # Return methods for variables

    def getClientIP(self):
        return self._conn.getpeername()

    # Returns the method of the request
    # e.g. GET, POST, PUT
    def getMethod(self):
        return self._method

    # Returns the body of request
    def getBody(self):
        return self._body

    # Returns the body of request
    def getJSONBody(self):
        if self._validateJSON(self._body):
            return json.loads(self._body)
        else:
            return False

    # Returns the path of request
    def getPath(self):
        return self._path

    # Returns the get params
    # e.g. ?v=zyx = {'v': 'xyz'}
    def getArguments(self):
        return self._args

    # Private functions
    def _prepareMessage(self, code, headers, body):
        message = 'HTTP/1.1 '

        httpCodeRes = self._utils.getHTTPMessage(code)
        message += str(code) + ' ' + httpCodeRes + '\r\n'

        # headers['Content-Type'] = 'application/json'
        headers['Server'] = 'Monser'
        headers['Accept'] = '*/*'

        for (type, value) in headers.items():
            message += type + ': ' + value + '\r\n'

        if body != '':
            message += '\r\n/h/e'
            message += body

        message += '\r\n/m/e';

        return message


    def _validateJSON(self, obj):
        if type(obj) == dict:
            obj = json.dumps(obj)

        try:
            jsonObject = json.loads(obj)
            return True
        except ValueError:
            return False
