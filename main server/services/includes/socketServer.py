#!/usr/bin/env python3
# https://gist.github.com/gnilchee/da85b495d4e932da91a3

import socket, sys, traceback, time, datetime, ssl, re
from base64 import b64encode
from services.includes.apiAuth import apiAuth
from services.includes.httpRequest import httpRequest

class socketServer:
    def __init__(self, port, router, useAuth = True):
        self._httpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._httpsServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._httpsServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._httpsServer = ssl.wrap_socket(
            self._httpsServer,
            keyfile='./services/includes/cert/server.key',
            certfile='./services/includes/cert/server.crt',
            server_side=True,
            ssl_version=ssl.PROTOCOL_TLSv1_2,
            ca_certs=None,
            do_handshake_on_connect=True,
            suppress_ragged_eofs=True,
            ciphers='ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK')
        self._port = port
        self._router = router
        self._useAuth = useAuth

    def runHTTPS(self):
        try:
            self._httpsServer.bind(('0.0.0.0', self._port))
            self._httpsServer.listen(5)
        except OSError as e:
            print('server failed to bing to port')
            return


        while True:
            check = False
            try:
                connection, address = self._httpsServer.accept()
                # print('Client ({}) connected'.format(address))
                chunkedMessage = []
                finished = False
                while finished is not True:
                    chunk = connection.recv(1024) # TODO IMPROVE THIS
                    chunkedMessage.append(chunk.decode()) #  or b'\r\n\r\n'  or (len(chunk) < 1024)
                    if b'\r\n/m/e' in chunk or b'{"hostname": ' in chunk or b'{"key": ' in chunk:
                        finished = True

                fullMessage = re.sub('(\/m\/e)|(\/h\/e)', '', ''.join(chunkedMessage))
                self.processMessage(fullMessage, connection)
            except:
                print("Connection failed with client")
                traceback.print_exc()
            finally:
                try:
                    connection.shutdown(2)
                    connection.close()
                except:
                    break

    def runHTTP(self):
        try:
            self._httpServer.bind(('0.0.0.0', self._port))
            self._httpServer.listen(5)
        except OSError as e:
            print('server failed to bing to port')
            return

        while True:
            try:
                connection, address = self._httpServer.accept()
                # print('Client ({}) connected'.format(address))
                chunkedMessage = []
                finished = False
                while finished is not True:
                    chunk = connection.recv(1024)
                    chunkedMessage.append(chunk)
                    if chunk == b'' in chunk:
                        finished = True
                        continue
                # self.processMessage(''.join(chunkedMessage), connection)
                self.processMessage(''.join(chunkedMessage), connection)
            except:
                print("Connection failed with client")
                traceback.print_exc()

    def processMessage(self, message, connection):
        if message.encode() == b'':
            # print('Client ({}) shutdown'.format(connection.getpeername()))
            connection.shutdown(2)
            connection.close()
            return False

        clientConnection = httpRequest(**{'client': connection})
        headers, body = message.split('\r\n\r\n')

        request = headers.split('\r\n')[0:1]
        rawClientParams = filter(None, message.split('\r\n')[1:])
        clientParams = {}

        for param in rawClientParams:
            if ":" not in param:
                continue
            key, value = param.split(':', 1)
            clientParams[key] = value

        contentType = clientParams.get('Content-Type')
        acceptType = clientParams.get('Accept')
        authCode = clientParams.get('Authorization')

        if (self.checkAuth(authCode) is not True and self._useAuth):
            clientConnection.sendJSON({"Code": 401, "Message": "Authorization Error: Incorrect or no password/user supplied"}, 401)
            # connection.send(b'HTTP/1.1 401 Unauthorized\r\nContent-Type: application/json; charset=UTF-8\r\n\r\n{"error": "Authorization Error: Incorrect or no password/user supplied"}')
            connection.shutdown(2)
            connection.close()
            return

        requestData = request[0].split(' ')
        requestType = requestData[0]
        requestURI = requestData[1]

        try:
            self._router.route(**{
                'headers': headers,
                'path': requestURI,
                'method': requestType.upper(),
                'body': body,
                'client': connection
            })
        except Exception as err:
            print(err)
            clientConnection.sendJSON({"Code": 500, "Message": "Internal Server Error: Check main server logs"}, 500)
            # connection.send(b'HTTP/1.1 500 Internal Server Error\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n/shrug')

    def checkAuth(self, auth):
        if auth is None:
            return False

        authType = auth.strip(' ')[:5].lower()

        if authType != "basic":
            return False

        userAuth = auth.strip(' ').split(' ')

        user = 'basicAuthTest123'
        passw = 'basicAuthTest123'
        combination = user + ":" + passw
        correct = b64encode(combination.encode()).decode()
        if userAuth[1] != correct:
            return False

        return True

# sample clientParams
# {
# 	'Host': ' localhost:2008',
# 	'Connection': ' keep-alive',
# 	'Postman-Token': ' a9d8ab89-af6b-47f3-f079-fa053988a3b2 ',
# 	'Cache - Control ': 'no - cache ',
# 	'Sec - Metadata ': 'destination = "", target = subresource,site = cross - site ',
# 	'User - Agent ': 'Mozilla / 5.0(Windows NT 6.3; Win64; x64) AppleWebKit / 537.36(KHTML,like Gecko) Chrome / 69.0 .3497 .100 Safari / 537.36 ',
# 	'Content - Type ': 'application / json ',
# 	'Accept ': ' */*',
# 	'Accept-Encoding': ' gzip, deflate, br',
# 	'Accept-Language': ' en-GB,en;q=0.9,en-US;q=0.8,pl;q=0.7,ja;q=0.6'
# }
