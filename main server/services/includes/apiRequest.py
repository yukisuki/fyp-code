#!/usr/bin/env python3

# Python std libs
import json

# Custom MonSer libs
from services.includes.httpUtils import httpUtils

class apiRequest:
    def __init__(self, **params):
        self._conn = params.get('client')
        self._method = params.get('method')
        self._path = params.get('path')
        self._args = params.get('args')
        self._body = params.get('body')
        self._utils = httpUtils()

    # message = self._prepareMessage(httpResponseCode, headers, body)
    def send(self, httpResponseCode = 200, headers = {}, body = ''):
        self._conn.send_response(httpResponseCode)
        self._conn.send_header('Content-type','plain/text')
        # for header in headers:
            # self._conn.self.send_header('Content-type','application/json')

        self._conn.end_headers()
        self._conn.wfile.write(bytes(body, "utf8"))


    # message = self._prepareMessage(httpResponseCode, {'Content-Type': 'application/json'}, json.dumps(jsonObject))
    def sendJSON(self, jsonObject, httpResponseCode = 200):
        if self._validateJSON(jsonObject):
            self._conn.send_response(httpResponseCode)
            self._conn.send_header('Content-type','application/json')
            self._conn.end_headers()

            message = json.dumps(jsonObject)
            self._conn.wfile.write(bytes(message, "utf8"))

            # self._conn.send(message.encode())
        else:
            self.internalServerError()
            print('JSON Data for request {} is invalid'.format(self._path))

    def badRequest(self, body):
        body = json.dumps({"errorCode": "400", "Message": body})
        self._conn.send_response(400)
        self._conn.send_header('Content-type','application/json')
        self._conn.end_headers()
        self._conn.wfile.write(bytes(body, "utf8"))

    def notFound(self):
        body = json.dumps({"errorCode": "404", "Message": "The requested path cannot be found"})
        self._conn.send_response(404)
        self._conn.send_header('Content-type','application/json')
        self._conn.end_headers()
        self._conn.wfile.write(bytes(body, "utf8"))


    def internalServerError(self):
        body = json.dumps({"errorCode": "500", "Message": "Problem occoured whilst trying to process the request"})
        self._conn.send_response(500)
        self._conn.send_header('Content-type','application/json')
        self._conn.end_headers()
        self._conn.wfile.write(bytes(body, "utf8"))

    # Return methods for variables

    def getClientIP(self):
        return self._conn.getpeername()

    # Returns the method of the request
    # e.g. GET, POST, PUT
    def getMethod(self):
        return self._method

    # Returns the body of request
    def getBody(self):
        return self._body

    # Returns the body of request
    def getJSONBody(self):
        if self._validateJSON(self._body):
            return json.loads(self._body)
        else:
            return False

    # Returns the path of request
    def getPath(self):
        return self._path

    # Returns the get params
    # e.g. ?v=zyx = {'v': 'xyz'}
    def getArguments(self):
        return self._args

    # Private functions
    def _prepareMessage(self, code, headers, body):
        message = 'HTTP/1.1 '

        httpCodeRes = self._utils.getHTTPMessage(code)
        message += str(code) + ' ' + httpCodeRes + '\r\n'

        # headers['Content-Type'] = 'application/json'
        headers['Server'] = 'Monser'
        headers['Accept'] = '*/*'

        for (type, value) in headers.items():
            message += type + ': ' + value + '\r\n'

        if body != '':
            message += '\r\n/h/e'
            message += body

        message += '\r\n/m/e';

        return message


    def _validateJSON(self, obj):
        if type(obj) == dict:
            obj = json.dumps(obj)

        try:
            jsonObject = json.loads(obj)
            return True
        except ValueError:
            return False
        except TypeError:
            return False
