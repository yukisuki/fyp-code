#!/usr/bin/env python3

import pymongo

class mongoDB:
    def __init__(self, url, databaseString):
        self.uri        = url
        self.database   = databaseString

    def getDatabaseNames(self):
        return self._db.list_database_names()

    def getCollectionNames(self):
        return self._db.list_collection_names()

    def insert(self, table, data):
        table = self._database[table]
        row = table.insert_one(data)
        return(row.inserted_id)

    def getFirstRow(self, table):
        table = self._database[table]
        return(table.find_one())

    def getOneRow(self, table, param = {}):
        table = self._database[table]
        results = table.find(param).limit(1)
        for result in results:
            return result

    def getRows(self, table, param = {}):
        table = self._database[table]
        results = table.find(param)
        return results

    def deleteRow(self, table, params = {}):
        table = self._database[table]
        if params is empty:
            raise ValueError('Params argument cannot be empty')

        table.delete_one(params)

    def deleteRows(self, table, params = {}):
        table = self._database[table]
        if params is None:
            raise ValueError('Params argument cannot be empty')

        result = table.delete_many(params)
        return(result.deleted_count)

    # URI property
    # used to init the base connection to monogdb
    @property
    def uri(self):
        return self._uri

    @uri.setter
    def uri(self, url):
        self._uri   = url
        self._db    = pymongo.MongoClient(url)

    @uri.deleter
    def uri(self):
        self._db.close()
        del self._uri


    @property
    def database(self):
        return self._database

    @database.setter
    def database(self, databaseString):
        self._database = self._db[databaseString]

    @database.deleter
    def database(self):
        del self._database


def runTest():
    mdb = mongoDB('mongodb://localhost:27017/', 'test')

    print(mdb.getDatabaseNames())

    print(mdb.insert('test', {"sample": "one"}))

    print(mdb.getFirstRow('test'))

    print(mdb.getOneRow('test', {"sample": "one"}))

    for record in mdb.getRows('test'):
        print(record)

    print(mdb.deleteRow('test', {"sample": "one"}))



if __name__ == "__main__":
    runTest()
