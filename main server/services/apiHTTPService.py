#!/usr/bin/env python3

# Python std libs
from threading import Thread
from http.server import HTTPServer, BaseHTTPRequestHandler
import ssl
import traceback

# Custom MonSer libs
from services.includes.apiHTTPDHandler import apiHTTPDHandler
from services.includes.apiRouter import apiRouter
from services.includes.mongoDB import mongoDB


class apiHTTPService(Thread):
    def __init__(self, apiPort):
        self._port = apiPort
        self._server = HTTPServer(('localhost', 4443), apiHandler) # baseAPIRequestHandler
        self._server.socket = ssl.wrap_socket(self._server.socket, certfile='./services/includes/cert/server.pem', server_side=True)
        Thread.__init__(self)

    def run(self):
        self._server.serve_forever()




class apiHandler(apiHTTPDHandler):
    _checkDB = mongoDB('mongodb://localhost:27017/', 'monser')
    _router = apiRouter()

    def _index(self, req):
        if req.getMethod() == 'POST':
            req.send(200, {}, 'Post to index')
            body = req.getJSONBody()

            print(self._checkDB)

            print(body["test"])

        else:
            req.send(200, {}, 'Get index')

    def _getHosts(self, req):
        req.send(200, {}, 'Getting hosts')

    def do_REQUEST(self):
        self._router.addRoute(self._index, **{'path': '/index', 'methods': ['POST', 'GET']})
        self._router.addRoute(self._getHosts, **{'path': '/hosts/all', 'methods': ['GET']})

        client = self

        body = None
        path = self.path
        method = self.command.lower()

        headers = {}
        rawHeaders = str(self.headers).split('\n')
        for head in rawHeaders:
            if head != '':
                key, val = head.split(': ')
                headers[key] = val

        if headers.get('Content-Length') is not None:
            body = self.rfile.read(int(headers.get('Content-Length'))).decode()

        try:
            self._router.route(**{
                'headers': headers,
                'path': path,
                'method': method.upper(),
                'body': body,
                'client': client
            })
        except Exception as err:
            traceback.print_exc()
            print(err)
        finally:
            return
