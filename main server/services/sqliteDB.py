#!/usr/bin/env python3

# Python std libs
from time import sleep
import random
import string
#import queue

# Custom MonSer libs
from services.includes.sqlitedb import sqlitedb

# Sqlite3 class
# This class is responsible for containg the
# information about the clients to make sure
# the client is who they say they are
class sqliteDB():

    # init the config db and sqlite lite
    def __init__(self):
        self._fileName = 'config/_config.db'
        #self._queue = queue.Queue()
        self._db = sqlitedb(self._fileName)
        self._setup()

    # creates new cli key for the client
    def createNewCLIKey(self, **data):
        table   = 'clients'
        key     = self._createKey()
        cliIP   = data['ip']
        cliName = data['name']

        data = self._db.getFirstRow(table, [["client_ip", "=", cliIP]])

        if data is not None:
            return False

        cliData = dict(
            client_name =cliName,
            client_ip   =cliIP,
            client_key  =key
        )

        self._db.insertRow(table, cliData)

        return key

    # check client key
    # checks client key against client ip and key in db
    def checkCLIKey(self, **data):
        table   = 'clients'
        cliIP   = data['ip']
        cliKey = data['key']

        data = self._db.getFirstRow(table, [["client_ip", "=", cliIP], ["client_key", "=", cliKey]])

        if data == None:
            return False

        return True

    # creates table data if not created
    def _setup(self):
        self._db.sql_commit('CREATE TABLE IF NOT EXISTS clients (id INTEGER PRIMARY KEY AUTOINCREMENT, client_name TEXT, client_ip TEXT, client_key TEXT)')

    #
    # def run(self):
    #     print('sqlite db service started')
    #
    #     while True:
    #         job = self._queue
    #         if not job.empty():
    #             print(job.get())
    #         else:
    #             sleep(.100)

    # create raw client key
    def _createKey(self):
        key = ''.join(random.choice(
            string.ascii_lowercase + string.ascii_uppercase + string.digits
            ) for _ in range(12))
        return key
